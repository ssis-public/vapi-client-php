# OpenAPIClient-php

<b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><br>[Changelog](https://vapi.ssis.de/public/changelog)

For more information, please visit [https://eln.de](https://eln.de).

## Installation & Usage

### Requirements

PHP 7.2 and later.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getNewToken();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->getNewToken: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://vapi.ssis.de*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AuthenticationApi* | [**getNewToken**](docs/Api/AuthenticationApi.md#getnewtoken) | **GET** /public/getNewToken | Get a new Token only if the given Token is valid. Otherwise a new login is required.
*AuthenticationApi* | [**login**](docs/Api/AuthenticationApi.md#login) | **POST** /public/login | Login the given API-User and return a Token.
*DealerApi* | [**getLoggedInDealerDetails**](docs/Api/DealerApi.md#getloggedindealerdetails) | **GET** /dealer/info | DealerApi.info()
*ImageManagerApi* | [**saveImage**](docs/Api/ImageManagerApi.md#saveimage) | **POST** /manage/image | Saves an image
*InquiriesApi* | [**create**](docs/Api/InquiriesApi.md#create) | **PUT** /inquiry/create | Create an inquiry.
*StaticValuesApi* | [**getAllStaticValueClasses**](docs/Api/StaticValuesApi.md#getallstaticvalueclasses) | **GET** /value | Get all class names for static values.
*StaticValuesApi* | [**getModelsByMake**](docs/Api/StaticValuesApi.md#getmodelsbymake) | **GET** /value/models/{make} | Get all models of given make.
*StaticValuesApi* | [**getStaticValueContentByKey**](docs/Api/StaticValuesApi.md#getstaticvaluecontentbykey) | **GET** /value/{staticValueClassName}/{staticValueKey} | Get values of given static value class and key.
*StaticValuesApi* | [**getStaticValueContents**](docs/Api/StaticValuesApi.md#getstaticvaluecontents) | **GET** /value/{staticValueClassName} | Get all values of given static value class.
*VehicleApi* | [**findAllVehicleIdsFromAuthUser**](docs/Api/VehicleApi.md#findallvehicleidsfromauthuser) | **GET** /vehicle | VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)
*VehicleApi* | [**findVehicleIds**](docs/Api/VehicleApi.md#findvehicleids) | **POST** /vehicle | VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)
*VehicleApi* | [**getVehicleAggregationTags**](docs/Api/VehicleApi.md#getvehicleaggregationtags) | **GET** /vehicle/aggregationtags/{tags} | VehicleApi.getVehicleAggregationTags(array[string] tags)
*VehicleApi* | [**getVehicleDetails**](docs/Api/VehicleApi.md#getvehicledetails) | **GET** /vehicle/details | VehicleApi.getVehicleDetails(array[string] uids)
*VehicleManagerApi* | [**deleteAllVehicles**](docs/Api/VehicleManagerApi.md#deleteallvehicles) | **DELETE** /manage/vehicle/all | delete all Vehicles
*VehicleManagerApi* | [**deleteAllVehiclesExcept**](docs/Api/VehicleManagerApi.md#deleteallvehiclesexcept) | **POST** /manage/vehicle/delete/except | delete all Vehicles except the ones with the given uids or offernumbers
*VehicleManagerApi* | [**deleteVehicleByOfferNumber**](docs/Api/VehicleManagerApi.md#deletevehiclebyoffernumber) | **DELETE** /manage/vehicle | delete the Vehicle with the given offerNumber
*VehicleManagerApi* | [**deleteVehicleByUid**](docs/Api/VehicleManagerApi.md#deletevehiclebyuid) | **DELETE** /manage/vehicle/{uid} | delete the Vehicle with the given vehicleUid
*VehicleManagerApi* | [**getVehicle**](docs/Api/VehicleManagerApi.md#getvehicle) | **GET** /manage/vehicle/{uid} | get the Vehicle with the given vehicleUid
*VehicleManagerApi* | [**getVehicleUids**](docs/Api/VehicleManagerApi.md#getvehicleuids) | **GET** /manage/vehicle | get a list of all vehicleUids that belong to the current Dealer
*VehicleManagerApi* | [**getVehicleUidsWithFlatParameters**](docs/Api/VehicleManagerApi.md#getvehicleuidswithflatparameters) | **GET** /manage/vehicle/flat_parameters | get a list of all vehicleUids that belong to the current Dealer
*VehicleManagerApi* | [**saveVehicle**](docs/Api/VehicleManagerApi.md#savevehicle) | **POST** /manage/vehicle | save a Vehicle

## Models

- [AuthenticationRequest](docs/Model/AuthenticationRequest.md)
- [BetweenDouble](docs/Model/BetweenDouble.md)
- [BetweenInteger](docs/Model/BetweenInteger.md)
- [BetweenLocalDate](docs/Model/BetweenLocalDate.md)
- [BetweenOffsetDateTime](docs/Model/BetweenOffsetDateTime.md)
- [BetweenShort](docs/Model/BetweenShort.md)
- [CalculationInfo](docs/Model/CalculationInfo.md)
- [Change](docs/Model/Change.md)
- [ClassListResponse](docs/Model/ClassListResponse.md)
- [ClientResponseCommonResponse](docs/Model/ClientResponseCommonResponse.md)
- [ClientResponseIndexDealerDetailResponseList](docs/Model/ClientResponseIndexDealerDetailResponseList.md)
- [ClientResponseIndexVehicleDetailResponseList](docs/Model/ClientResponseIndexVehicleDetailResponseList.md)
- [ClientResponseIndexVehicleSearchResponseList](docs/Model/ClientResponseIndexVehicleSearchResponseList.md)
- [ClientResponseManagerVehicle](docs/Model/ClientResponseManagerVehicle.md)
- [ClientResponseObject](docs/Model/ClientResponseObject.md)
- [ClientResponseTagSearchResult](docs/Model/ClientResponseTagSearchResult.md)
- [ClientResponseVehicleSaveResponse](docs/Model/ClientResponseVehicleSaveResponse.md)
- [ClientResponseVehicleUidList](docs/Model/ClientResponseVehicleUidList.md)
- [CommonResponse](docs/Model/CommonResponse.md)
- [DeleteVehiclesByUidOffernumberExceptRequest](docs/Model/DeleteVehiclesByUidOffernumberExceptRequest.md)
- [Error](docs/Model/Error.md)
- [ErrorDetails](docs/Model/ErrorDetails.md)
- [Exterior](docs/Model/Exterior.md)
- [Feature](docs/Model/Feature.md)
- [GeneralResponse](docs/Model/GeneralResponse.md)
- [Geolocation](docs/Model/Geolocation.md)
- [Image](docs/Model/Image.md)
- [IndexDealer](docs/Model/IndexDealer.md)
- [IndexDealerContact](docs/Model/IndexDealerContact.md)
- [IndexDealerCooperation](docs/Model/IndexDealerCooperation.md)
- [IndexDealerCooperationOption](docs/Model/IndexDealerCooperationOption.md)
- [IndexDealerCooperationStatus](docs/Model/IndexDealerCooperationStatus.md)
- [IndexDealerDetailResponseList](docs/Model/IndexDealerDetailResponseList.md)
- [IndexDealerProduct](docs/Model/IndexDealerProduct.md)
- [IndexDealerProductOption](docs/Model/IndexDealerProductOption.md)
- [IndexDealerProductStatus](docs/Model/IndexDealerProductStatus.md)
- [IndexDealerSetting](docs/Model/IndexDealerSetting.md)
- [IndexVehicle](docs/Model/IndexVehicle.md)
- [IndexVehicleDetailResponseList](docs/Model/IndexVehicleDetailResponseList.md)
- [IndexVehicleExterior](docs/Model/IndexVehicleExterior.md)
- [IndexVehicleFeature](docs/Model/IndexVehicleFeature.md)
- [IndexVehicleImage](docs/Model/IndexVehicleImage.md)
- [IndexVehicleSearchRequest](docs/Model/IndexVehicleSearchRequest.md)
- [IndexVehicleSearchResponseItem](docs/Model/IndexVehicleSearchResponseItem.md)
- [IndexVehicleSearchResponseList](docs/Model/IndexVehicleSearchResponseList.md)
- [IndexVehicleWarranty](docs/Model/IndexVehicleWarranty.md)
- [InquiryFree](docs/Model/InquiryFree.md)
- [InquiryRequest](docs/Model/InquiryRequest.md)
- [InquiryRequestor](docs/Model/InquiryRequestor.md)
- [InquiryVehicle](docs/Model/InquiryVehicle.md)
- [JWTTokenResponse](docs/Model/JWTTokenResponse.md)
- [ListResponseClientResponseIntf](docs/Model/ListResponseClientResponseIntf.md)
- [ManagerVehicle](docs/Model/ManagerVehicle.md)
- [ManagerVehicleFilterParameters](docs/Model/ManagerVehicleFilterParameters.md)
- [MetaData](docs/Model/MetaData.md)
- [PaginationParameters](docs/Model/PaginationParameters.md)
- [TagObject](docs/Model/TagObject.md)
- [TagSearchResult](docs/Model/TagSearchResult.md)
- [VehicleExteriorSearch](docs/Model/VehicleExteriorSearch.md)
- [VehicleFilter](docs/Model/VehicleFilter.md)
- [VehicleModelSearch](docs/Model/VehicleModelSearch.md)
- [VehicleSaveResponse](docs/Model/VehicleSaveResponse.md)
- [VehicleUidList](docs/Model/VehicleUidList.md)
- [VehicleUidListItem](docs/Model/VehicleUidListItem.md)

## Authorization

### bearerAuth

- **Type**: Bearer authentication (JWT)

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

technik@ssis.de

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `6.3.0-374`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`

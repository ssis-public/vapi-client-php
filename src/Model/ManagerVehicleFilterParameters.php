<?php
/**
 * ManagerVehicleFilterParameters
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SSIS\Vapi\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * VAPI
 *
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace SSIS\Vapi\Client\Model;

use \ArrayAccess;
use \SSIS\Vapi\Client\ObjectSerializer;

/**
 * ManagerVehicleFilterParameters Class Doc Comment
 *
 * @category Class
 * @package  SSIS\Vapi\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class ManagerVehicleFilterParameters implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'ManagerVehicleFilterParameters';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'offer_number' => 'string',
        'changed_before' => '\DateTime',
        'changed_after' => '\DateTime',
        'send_before' => '\DateTime',
        'send_after' => '\DateTime',
        'has_errors' => 'bool',
        'make_key' => 'string',
        'model_key' => 'string',
        'min_dealer_price' => 'float',
        'max_dealer_price' => 'float',
        'min_consumer_price' => 'float',
        'max_consumer_price' => 'float'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'offer_number' => null,
        'changed_before' => 'date-time',
        'changed_after' => 'date-time',
        'send_before' => 'date-time',
        'send_after' => 'date-time',
        'has_errors' => null,
        'make_key' => null,
        'model_key' => null,
        'min_dealer_price' => null,
        'max_dealer_price' => null,
        'min_consumer_price' => null,
        'max_consumer_price' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'offer_number' => 'offerNumber',
        'changed_before' => 'changedBefore',
        'changed_after' => 'changedAfter',
        'send_before' => 'sendBefore',
        'send_after' => 'sendAfter',
        'has_errors' => 'hasErrors',
        'make_key' => 'makeKey',
        'model_key' => 'modelKey',
        'min_dealer_price' => 'minDealerPrice',
        'max_dealer_price' => 'maxDealerPrice',
        'min_consumer_price' => 'minConsumerPrice',
        'max_consumer_price' => 'maxConsumerPrice'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'offer_number' => 'setOfferNumber',
        'changed_before' => 'setChangedBefore',
        'changed_after' => 'setChangedAfter',
        'send_before' => 'setSendBefore',
        'send_after' => 'setSendAfter',
        'has_errors' => 'setHasErrors',
        'make_key' => 'setMakeKey',
        'model_key' => 'setModelKey',
        'min_dealer_price' => 'setMinDealerPrice',
        'max_dealer_price' => 'setMaxDealerPrice',
        'min_consumer_price' => 'setMinConsumerPrice',
        'max_consumer_price' => 'setMaxConsumerPrice'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'offer_number' => 'getOfferNumber',
        'changed_before' => 'getChangedBefore',
        'changed_after' => 'getChangedAfter',
        'send_before' => 'getSendBefore',
        'send_after' => 'getSendAfter',
        'has_errors' => 'getHasErrors',
        'make_key' => 'getMakeKey',
        'model_key' => 'getModelKey',
        'min_dealer_price' => 'getMinDealerPrice',
        'max_dealer_price' => 'getMaxDealerPrice',
        'min_consumer_price' => 'getMinConsumerPrice',
        'max_consumer_price' => 'getMaxConsumerPrice'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['offer_number'] = $data['offer_number'] ?? null;
        $this->container['changed_before'] = $data['changed_before'] ?? null;
        $this->container['changed_after'] = $data['changed_after'] ?? null;
        $this->container['send_before'] = $data['send_before'] ?? null;
        $this->container['send_after'] = $data['send_after'] ?? null;
        $this->container['has_errors'] = $data['has_errors'] ?? null;
        $this->container['make_key'] = $data['make_key'] ?? null;
        $this->container['model_key'] = $data['model_key'] ?? null;
        $this->container['min_dealer_price'] = $data['min_dealer_price'] ?? null;
        $this->container['max_dealer_price'] = $data['max_dealer_price'] ?? null;
        $this->container['min_consumer_price'] = $data['min_consumer_price'] ?? null;
        $this->container['max_consumer_price'] = $data['max_consumer_price'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets offer_number
     *
     * @return string|null
     */
    public function getOfferNumber()
    {
        return $this->container['offer_number'];
    }

    /**
     * Sets offer_number
     *
     * @param string|null $offer_number offer_number
     *
     * @return self
     */
    public function setOfferNumber($offer_number)
    {
        $this->container['offer_number'] = $offer_number;

        return $this;
    }

    /**
     * Gets changed_before
     *
     * @return \DateTime|null
     */
    public function getChangedBefore()
    {
        return $this->container['changed_before'];
    }

    /**
     * Sets changed_before
     *
     * @param \DateTime|null $changed_before changed_before
     *
     * @return self
     */
    public function setChangedBefore($changed_before)
    {
        $this->container['changed_before'] = $changed_before;

        return $this;
    }

    /**
     * Gets changed_after
     *
     * @return \DateTime|null
     */
    public function getChangedAfter()
    {
        return $this->container['changed_after'];
    }

    /**
     * Sets changed_after
     *
     * @param \DateTime|null $changed_after changed_after
     *
     * @return self
     */
    public function setChangedAfter($changed_after)
    {
        $this->container['changed_after'] = $changed_after;

        return $this;
    }

    /**
     * Gets send_before
     *
     * @return \DateTime|null
     */
    public function getSendBefore()
    {
        return $this->container['send_before'];
    }

    /**
     * Sets send_before
     *
     * @param \DateTime|null $send_before send_before
     *
     * @return self
     */
    public function setSendBefore($send_before)
    {
        $this->container['send_before'] = $send_before;

        return $this;
    }

    /**
     * Gets send_after
     *
     * @return \DateTime|null
     */
    public function getSendAfter()
    {
        return $this->container['send_after'];
    }

    /**
     * Sets send_after
     *
     * @param \DateTime|null $send_after send_after
     *
     * @return self
     */
    public function setSendAfter($send_after)
    {
        $this->container['send_after'] = $send_after;

        return $this;
    }

    /**
     * Gets has_errors
     *
     * @return bool|null
     */
    public function getHasErrors()
    {
        return $this->container['has_errors'];
    }

    /**
     * Sets has_errors
     *
     * @param bool|null $has_errors has_errors
     *
     * @return self
     */
    public function setHasErrors($has_errors)
    {
        $this->container['has_errors'] = $has_errors;

        return $this;
    }

    /**
     * Gets make_key
     *
     * @return string|null
     */
    public function getMakeKey()
    {
        return $this->container['make_key'];
    }

    /**
     * Sets make_key
     *
     * @param string|null $make_key make_key
     *
     * @return self
     */
    public function setMakeKey($make_key)
    {
        $this->container['make_key'] = $make_key;

        return $this;
    }

    /**
     * Gets model_key
     *
     * @return string|null
     */
    public function getModelKey()
    {
        return $this->container['model_key'];
    }

    /**
     * Sets model_key
     *
     * @param string|null $model_key model_key
     *
     * @return self
     */
    public function setModelKey($model_key)
    {
        $this->container['model_key'] = $model_key;

        return $this;
    }

    /**
     * Gets min_dealer_price
     *
     * @return float|null
     */
    public function getMinDealerPrice()
    {
        return $this->container['min_dealer_price'];
    }

    /**
     * Sets min_dealer_price
     *
     * @param float|null $min_dealer_price min_dealer_price
     *
     * @return self
     */
    public function setMinDealerPrice($min_dealer_price)
    {
        $this->container['min_dealer_price'] = $min_dealer_price;

        return $this;
    }

    /**
     * Gets max_dealer_price
     *
     * @return float|null
     */
    public function getMaxDealerPrice()
    {
        return $this->container['max_dealer_price'];
    }

    /**
     * Sets max_dealer_price
     *
     * @param float|null $max_dealer_price max_dealer_price
     *
     * @return self
     */
    public function setMaxDealerPrice($max_dealer_price)
    {
        $this->container['max_dealer_price'] = $max_dealer_price;

        return $this;
    }

    /**
     * Gets min_consumer_price
     *
     * @return float|null
     */
    public function getMinConsumerPrice()
    {
        return $this->container['min_consumer_price'];
    }

    /**
     * Sets min_consumer_price
     *
     * @param float|null $min_consumer_price min_consumer_price
     *
     * @return self
     */
    public function setMinConsumerPrice($min_consumer_price)
    {
        $this->container['min_consumer_price'] = $min_consumer_price;

        return $this;
    }

    /**
     * Gets max_consumer_price
     *
     * @return float|null
     */
    public function getMaxConsumerPrice()
    {
        return $this->container['max_consumer_price'];
    }

    /**
     * Sets max_consumer_price
     *
     * @param float|null $max_consumer_price max_consumer_price
     *
     * @return self
     */
    public function setMaxConsumerPrice($max_consumer_price)
    {
        $this->container['max_consumer_price'] = $max_consumer_price;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}



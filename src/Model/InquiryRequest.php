<?php
/**
 * InquiryRequest
 *
 * PHP version 7.2
 *
 * @category Class
 * @package  SSIS\Vapi\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * VAPI
 *
 * <b>Build Time: 2022-12-09T17:30:18.023+01:00</b><br><p><a href=\"/public/changelog\" target=\"_blank\">Changelog</a></p><p class=\"clients\">API-Clients:<br><a href=\"https://gitlab.com/ssis-public/vapi-client-php\" target=\"_blank\">PHP-Client</a><br><a href=\"https://gitlab.com/ssis-public/vapi-client-java\" target=\"_blank\">Java-Client</a></p>
 *
 * The version of the OpenAPI document: 6.3.0-374
 * Contact: technik@ssis.de
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.1.1
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace SSIS\Vapi\Client\Model;

use \ArrayAccess;
use \SSIS\Vapi\Client\ObjectSerializer;

/**
 * InquiryRequest Class Doc Comment
 *
 * @category Class
 * @description The request class for create new inquiry.
 * @package  SSIS\Vapi\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class InquiryRequest implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'InquiryRequest';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'requestor_dealer_id' => 'int',
        'requested_dealer_id' => 'int',
        'inquiry_type' => 'string',
        'inquiry_subtype' => 'string',
        'inquiry_requestor' => '\SSIS\Vapi\Client\Model\InquiryRequestor',
        'inquiry_vehicle' => '\SSIS\Vapi\Client\Model\InquiryVehicle',
        'inquiry_free' => '\SSIS\Vapi\Client\Model\InquiryFree',
        'time_of_purchase' => 'string',
        'customer_message' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'requestor_dealer_id' => 'int64',
        'requested_dealer_id' => 'int64',
        'inquiry_type' => null,
        'inquiry_subtype' => null,
        'inquiry_requestor' => null,
        'inquiry_vehicle' => null,
        'inquiry_free' => null,
        'time_of_purchase' => null,
        'customer_message' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'requestor_dealer_id' => 'requestorDealerId',
        'requested_dealer_id' => 'requestedDealerId',
        'inquiry_type' => 'inquiryType',
        'inquiry_subtype' => 'inquirySubtype',
        'inquiry_requestor' => 'inquiryRequestor',
        'inquiry_vehicle' => 'inquiryVehicle',
        'inquiry_free' => 'inquiryFree',
        'time_of_purchase' => 'timeOfPurchase',
        'customer_message' => 'customerMessage'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'requestor_dealer_id' => 'setRequestorDealerId',
        'requested_dealer_id' => 'setRequestedDealerId',
        'inquiry_type' => 'setInquiryType',
        'inquiry_subtype' => 'setInquirySubtype',
        'inquiry_requestor' => 'setInquiryRequestor',
        'inquiry_vehicle' => 'setInquiryVehicle',
        'inquiry_free' => 'setInquiryFree',
        'time_of_purchase' => 'setTimeOfPurchase',
        'customer_message' => 'setCustomerMessage'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'requestor_dealer_id' => 'getRequestorDealerId',
        'requested_dealer_id' => 'getRequestedDealerId',
        'inquiry_type' => 'getInquiryType',
        'inquiry_subtype' => 'getInquirySubtype',
        'inquiry_requestor' => 'getInquiryRequestor',
        'inquiry_vehicle' => 'getInquiryVehicle',
        'inquiry_free' => 'getInquiryFree',
        'time_of_purchase' => 'getTimeOfPurchase',
        'customer_message' => 'getCustomerMessage'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['requestor_dealer_id'] = $data['requestor_dealer_id'] ?? null;
        $this->container['requested_dealer_id'] = $data['requested_dealer_id'] ?? null;
        $this->container['inquiry_type'] = $data['inquiry_type'] ?? 'INQUIRY';
        $this->container['inquiry_subtype'] = $data['inquiry_subtype'] ?? null;
        $this->container['inquiry_requestor'] = $data['inquiry_requestor'] ?? null;
        $this->container['inquiry_vehicle'] = $data['inquiry_vehicle'] ?? null;
        $this->container['inquiry_free'] = $data['inquiry_free'] ?? null;
        $this->container['time_of_purchase'] = $data['time_of_purchase'] ?? null;
        $this->container['customer_message'] = $data['customer_message'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['inquiry_type'] === null) {
            $invalidProperties[] = "'inquiry_type' can't be null";
        }
        if ($this->container['inquiry_subtype'] === null) {
            $invalidProperties[] = "'inquiry_subtype' can't be null";
        }
        if ($this->container['inquiry_requestor'] === null) {
            $invalidProperties[] = "'inquiry_requestor' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets requestor_dealer_id
     *
     * @return int|null
     */
    public function getRequestorDealerId()
    {
        return $this->container['requestor_dealer_id'];
    }

    /**
     * Sets requestor_dealer_id
     *
     * @param int|null $requestor_dealer_id Leave empty, if the request comes from end customer.
     *
     * @return self
     */
    public function setRequestorDealerId($requestor_dealer_id)
    {
        $this->container['requestor_dealer_id'] = $requestor_dealer_id;

        return $this;
    }

    /**
     * Gets requested_dealer_id
     *
     * @return int|null
     */
    public function getRequestedDealerId()
    {
        return $this->container['requested_dealer_id'];
    }

    /**
     * Sets requested_dealer_id
     *
     * @param int|null $requested_dealer_id Leave empty, if the request goes to all dealers.
     *
     * @return self
     */
    public function setRequestedDealerId($requested_dealer_id)
    {
        $this->container['requested_dealer_id'] = $requested_dealer_id;

        return $this;
    }

    /**
     * Gets inquiry_type
     *
     * @return string
     */
    public function getInquiryType()
    {
        return $this->container['inquiry_type'];
    }

    /**
     * Sets inquiry_type
     *
     * @param string $inquiry_type The inquiry type. Possible values: INQUIRY, OFFER
     *
     * @return self
     */
    public function setInquiryType($inquiry_type)
    {
        $this->container['inquiry_type'] = $inquiry_type;

        return $this;
    }

    /**
     * Gets inquiry_subtype
     *
     * @return string
     */
    public function getInquirySubtype()
    {
        return $this->container['inquiry_subtype'];
    }

    /**
     * Sets inquiry_subtype
     *
     * @param string $inquiry_subtype The inquiry subtype. Possible values: INQUIRY_DIRECT_PUBLIC, INQUIRY_DIRECT_INTERN, INQUIRY_FREE_PUBLIC, INQUIRY_FREE_INTERN
     *
     * @return self
     */
    public function setInquirySubtype($inquiry_subtype)
    {
        $this->container['inquiry_subtype'] = $inquiry_subtype;

        return $this;
    }

    /**
     * Gets inquiry_requestor
     *
     * @return \SSIS\Vapi\Client\Model\InquiryRequestor
     */
    public function getInquiryRequestor()
    {
        return $this->container['inquiry_requestor'];
    }

    /**
     * Sets inquiry_requestor
     *
     * @param \SSIS\Vapi\Client\Model\InquiryRequestor $inquiry_requestor inquiry_requestor
     *
     * @return self
     */
    public function setInquiryRequestor($inquiry_requestor)
    {
        $this->container['inquiry_requestor'] = $inquiry_requestor;

        return $this;
    }

    /**
     * Gets inquiry_vehicle
     *
     * @return \SSIS\Vapi\Client\Model\InquiryVehicle|null
     */
    public function getInquiryVehicle()
    {
        return $this->container['inquiry_vehicle'];
    }

    /**
     * Sets inquiry_vehicle
     *
     * @param \SSIS\Vapi\Client\Model\InquiryVehicle|null $inquiry_vehicle inquiry_vehicle
     *
     * @return self
     */
    public function setInquiryVehicle($inquiry_vehicle)
    {
        $this->container['inquiry_vehicle'] = $inquiry_vehicle;

        return $this;
    }

    /**
     * Gets inquiry_free
     *
     * @return \SSIS\Vapi\Client\Model\InquiryFree|null
     */
    public function getInquiryFree()
    {
        return $this->container['inquiry_free'];
    }

    /**
     * Sets inquiry_free
     *
     * @param \SSIS\Vapi\Client\Model\InquiryFree|null $inquiry_free inquiry_free
     *
     * @return self
     */
    public function setInquiryFree($inquiry_free)
    {
        $this->container['inquiry_free'] = $inquiry_free;

        return $this;
    }

    /**
     * Gets time_of_purchase
     *
     * @return string|null
     */
    public function getTimeOfPurchase()
    {
        return $this->container['time_of_purchase'];
    }

    /**
     * Sets time_of_purchase
     *
     * @param string|null $time_of_purchase When would the interested person like to buy the vehicle.
     *
     * @return self
     */
    public function setTimeOfPurchase($time_of_purchase)
    {
        $this->container['time_of_purchase'] = $time_of_purchase;

        return $this;
    }

    /**
     * Gets customer_message
     *
     * @return string|null
     */
    public function getCustomerMessage()
    {
        return $this->container['customer_message'];
    }

    /**
     * Sets customer_message
     *
     * @param string|null $customer_message An additional message from the customer.
     *
     * @return self
     */
    public function setCustomerMessage($customer_message)
    {
        $this->container['customer_message'] = $customer_message;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}



<?php
namespace SSIS\Vapi\Client;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;

use SSIS\Vapi\Client\Api\AuthenticationApi;
use SSIS\Vapi\Client\Model\AuthenticationRequest;

/**
 * 
 * @author der@ssis.de
 *
 */
class VapiClientFactory
{
    const DEFAULT_MIN_TOKEN_LIFETIME = 3600;
    const CLOCK_SKEW = 60;
    
    private $username;
    private $password;
    private $client;
    private $configuration;
    private $headerSelector;
    private $hostIndex;
    private $minTokenLifetime;
    
    private $api = [];
    
    /**
     * @param  string $username
     * @param  string $password
     * @return VapiClientFactory
     */
    public static function createFromConfigFile(string $configFile, string $token = null) : VapiClientFactory {
        $config = require $configFile;
        return new self(
            array_key_exists('username',         $config) ? $config['username']         : null,
            array_key_exists('password',         $config) ? $config['password']         : null,
            $token,
            array_key_exists('client',           $config) ? $config['client']           : null,
            array_key_exists('config',           $config) ? $config['config']           : null,
            array_key_exists('selector',         $config) ? $config['selector']         : null,
            array_key_exists('hostIndex',        $config) ? $config['hostIndex']        : 0,
            array_key_exists('minTokenLifetime', $config) ? $config['minTokenLifetime'] : null
        );
    }
    
    /**
     * @param string          $username
     * @param string          $password
     * @param string          $token
     * @param ClientInterface $client
     * @param Configuration   $config
     * @param HeaderSelector  $selector
     * @param int             $hostIndex
     */
    public function __construct(
        string          $username         = null,
        string          $password         = null,
        string          $token            = null,
        ClientInterface $client           = null,
        Configuration   $config           = null,
        HeaderSelector  $selector         = null,
        int             $hostIndex        = 0,
		int             $minTokenLifetime = null
    ) {
        $this->username         = $username;
        $this->password         = $password;
        if (!empty($token)) {
            $this->configuration->setAccessToken($token);
        }
        $this->client           = $client   ?: new Client();
        $this->configuration    = $config   ?: new Configuration();
        $this->headerSelector   = $selector ?: new HeaderSelector();
        $this->hostIndex        = $hostIndex;
        $this->minTokenLifetime = null !== $minTokenLifetime ? $minTokenLifetime : self::DEFAULT_MIN_TOKEN_LIFETIME;
    }
    
    /**
	 * gets an intitalised and logged in API
	 *
     * @param  class   an API Class
     * @param  boolean lazy *optional* if lazy is truthy, no attempt to renew the token will be made
     * @return object  an initialised instance of the requested API Class
     */
    public function getApi($class, $lazy = false)
    {
        if (!array_key_exists($class, $this->api)) {
            $this->api[$class] = new $class($this->client, $this->configuration, $this->headerSelector, $this->hostIndex);
            if  (!$lazy && !($this->getTokenRemainingLifetime() < $this->minTokenLifetime)) {
                $this->renewToken();
            }
        }
        return $this->api[$class];
    }
    
    
    /**
	 * gets the remaining life time of the curently used access token in seconds
     * @return int remaining life time in seconds
     */
    public function getTokenRemainingLifetime(): int
    {
        $token = $this->configuration->getAccessToken();
        if (empty($token)) { return -1; }
        $tokenParts = explode('.', $token);
        if (count($tokenParts) !== 3) { return -1; }
        return json_decode(base64_decode($tokenParts[1]))->exp - time() - self::CLOCK_SKEW;
    }
    
    /**
	 * checks if the remaining token lifetime is grater 0
     * @return bool 
     */
    public function tokenIsValid(): bool
    {
        return $this->getTokenRemainingLifetime() >= 0;
    }
    
    /**
	 * renews the token. using AuthenticationApi::getNewToken() or AuthenticationApi::getToken()
     * @return string
     */
    public function renewToken(): string
    {
        if ($this->tokenIsValid()) {
            $newToken = (new AuthenticationApi(
                $this->client,
                $this->configuration,
                $this->headerSelector,
                $this->hostIndex
                ))->getNewToken()->getToken();
            
        } else {
            $newToken = (new AuthenticationApi(
                $this->client,
                $this->configuration,
                $this->headerSelector,
                $this->hostIndex
            ))->login(new AuthenticationRequest([
                'name'     => $this->username,
                'password' => $this->password,
            ]))->getToken();
        }
        $this->configuration->setAccessToken($newToken);
        return $newToken;
    }
    
    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }
    
    /**
     * @param string $username
     */
    public function setUsername(string $username)
    {
        $this->username = $username;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }
    
    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }
    
    /**
     * @return ?string
     */
    public function getToken(): ?string
    {
        return $this->configuration->getAccessToken();
    }
    
    /**
     * @param string $password
     */
    public function setToken(?string $token)
    {
        $this->configuration->setAccessToken($token);
        return $this;
    }
    
    /**
     * @return \GuzzleHttp\ClientInterface
     */
    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    /**
     * @param \GuzzleHttp\ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
        return $this;
    }
    
    /**
     * @return \Swagger\Client\Configuration
     */
    public function getConfig(): Configuration
    {
        return $this->configuration;
    }
    
    /**
     * @param \Swagger\Client\Configuration $config
     * @return \VapiClientFactory
     */
    public function setConfig(Configuration $config): VapiClientFactory
    {
        $this->configuration = $config;
        return $this;
    }

    /**
     * @return \Swagger\Client\HeaderSelector
     */
    public function getHeaderSelector(): HeaderSelector
    {
        return $this->headerSelector;
    }

    /**
     * @param \Swagger\Client\HeaderSelector $headerSelector
     */
    public function setHeaderSelector(HeaderSelector $headerSelector): VapiClientFactory
    {
        $this->headerSelector = $headerSelector;
        return $this;
    }

    /**
     * @return int
     */
    public function getHostIndex(): int
    {
        return $this->hostIndex;
    }

    /**
     * @param int $hostIndex
     */
    public function setHostIndex(int $hostIndex): VapiClientFactory
    {
        $this->hostIndex = $hostIndex;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinTokenLifetime(): int
    {
        return $this->minTokenLifetime;
    }

    /**
     * @param int $hostIndex
     */
    public function setMinTokenLifetime(int $minTokenLifetime): VapiClientFactory
    {
        $this->minTokenLifetime = $minTokenLifetime;
        return $this;
    }

}

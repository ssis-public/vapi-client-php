# SSIS\Vapi\Client\InquiriesApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**create()**](InquiriesApi.md#create) | **PUT** /inquiry/create | Create an inquiry.


## `create()`

```php
create($inquiry_request): \SSIS\Vapi\Client\Model\ClientResponseCommonResponse
```

Create an inquiry.

Create an inquiry.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\InquiriesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$inquiry_request = new \SSIS\Vapi\Client\Model\InquiryRequest(); // \SSIS\Vapi\Client\Model\InquiryRequest | Create an inquiry.

try {
    $result = $apiInstance->create($inquiry_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling InquiriesApi->create: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inquiry_request** | [**\SSIS\Vapi\Client\Model\InquiryRequest**](../Model/InquiryRequest.md)| Create an inquiry. |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseCommonResponse**](../Model/ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

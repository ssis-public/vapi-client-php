# SSIS\Vapi\Client\VehicleApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**findAllVehicleIdsFromAuthUser()**](VehicleApi.md#findAllVehicleIdsFromAuthUser) | **GET** /vehicle | VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)
[**findVehicleIds()**](VehicleApi.md#findVehicleIds) | **POST** /vehicle | VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)
[**getVehicleAggregationTags()**](VehicleApi.md#getVehicleAggregationTags) | **GET** /vehicle/aggregationtags/{tags} | VehicleApi.getVehicleAggregationTags(array[string] tags)
[**getVehicleDetails()**](VehicleApi.md#getVehicleDetails) | **GET** /vehicle/details | VehicleApi.getVehicleDetails(array[string] uids)


## `findAllVehicleIdsFromAuthUser()`

```php
findAllVehicleIdsFromAuthUser($result_count, $result_offset, $sort_type, $sort_order_asc): \SSIS\Vapi\Client\Model\ClientResponseIndexVehicleSearchResponseList
```

VehicleApi.findAllVehicleIdsFromAuthUser(integer resultCount, integer resultOffset, string sortType, boolean sortOrderASC)

Get all your vehicles.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$result_count = 10; // int
$result_offset = 0; // int
$sort_type = 'uid'; // string
$sort_order_asc = true; // bool

try {
    $result = $apiInstance->findAllVehicleIdsFromAuthUser($result_count, $result_offset, $sort_type, $sort_order_asc);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleApi->findAllVehicleIdsFromAuthUser: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **result_count** | **int**|  | [optional] [default to 10]
 **result_offset** | **int**|  | [optional] [default to 0]
 **sort_type** | **string**|  | [optional] [default to &#39;uid&#39;]
 **sort_order_asc** | **bool**|  | [optional] [default to true]

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseIndexVehicleSearchResponseList**](../Model/ClientResponseIndexVehicleSearchResponseList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `findVehicleIds()`

```php
findVehicleIds($index_vehicle_search_request): \SSIS\Vapi\Client\Model\ClientResponseIndexVehicleSearchResponseList
```

VehicleApi.findVehicleIds(IndexVehicleSearchRequest indexVehicleSearchRequest)

Search for vehicles.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$index_vehicle_search_request = new \SSIS\Vapi\Client\Model\IndexVehicleSearchRequest(); // \SSIS\Vapi\Client\Model\IndexVehicleSearchRequest

try {
    $result = $apiInstance->findVehicleIds($index_vehicle_search_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleApi->findVehicleIds: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **index_vehicle_search_request** | [**\SSIS\Vapi\Client\Model\IndexVehicleSearchRequest**](../Model/IndexVehicleSearchRequest.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseIndexVehicleSearchResponseList**](../Model/ClientResponseIndexVehicleSearchResponseList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVehicleAggregationTags()`

```php
getVehicleAggregationTags($tags): \SSIS\Vapi\Client\Model\ClientResponseTagSearchResult
```

VehicleApi.getVehicleAggregationTags(array[string] tags)

Get vehicle aggregation tags. Send 'ALL' for an tag overview.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$tags = array('tags_example'); // string[]

try {
    $result = $apiInstance->getVehicleAggregationTags($tags);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleApi->getVehicleAggregationTags: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tags** | [**string[]**](../Model/string.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseTagSearchResult**](../Model/ClientResponseTagSearchResult.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVehicleDetails()`

```php
getVehicleDetails($uids): \SSIS\Vapi\Client\Model\ClientResponseIndexVehicleDetailResponseList
```

VehicleApi.getVehicleDetails(array[string] uids)

Get details for several vehicles.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uids = array('uids_example'); // string[]

try {
    $result = $apiInstance->getVehicleDetails($uids);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleApi->getVehicleDetails: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uids** | [**string[]**](../Model/string.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseIndexVehicleDetailResponseList**](../Model/ClientResponseIndexVehicleDetailResponseList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

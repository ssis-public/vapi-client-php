# SSIS\Vapi\Client\ImageManagerApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**saveImage()**](ImageManagerApi.md#saveImage) | **POST** /manage/image | Saves an image


## `saveImage()`

```php
saveImage($file, $branded): \SSIS\Vapi\Client\Model\ClientResponseCommonResponse
```

Saves an image

Saves an image to the image pool of the current Dealer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\ImageManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$file = "/path/to/file.txt"; // \SplFileObject
$branded = True; // bool

try {
    $result = $apiInstance->saveImage($file, $branded);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ImageManagerApi->saveImage: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **\SplFileObject****\SplFileObject**|  |
 **branded** | **bool**|  | [optional]

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseCommonResponse**](../Model/ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `multipart/form-data`
- **Accept**: `*/*`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

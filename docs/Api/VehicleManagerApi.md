# SSIS\Vapi\Client\VehicleManagerApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAllVehicles()**](VehicleManagerApi.md#deleteAllVehicles) | **DELETE** /manage/vehicle/all | delete all Vehicles
[**deleteAllVehiclesExcept()**](VehicleManagerApi.md#deleteAllVehiclesExcept) | **POST** /manage/vehicle/delete/except | delete all Vehicles except the ones with the given uids or offernumbers
[**deleteVehicleByOfferNumber()**](VehicleManagerApi.md#deleteVehicleByOfferNumber) | **DELETE** /manage/vehicle | delete the Vehicle with the given offerNumber
[**deleteVehicleByUid()**](VehicleManagerApi.md#deleteVehicleByUid) | **DELETE** /manage/vehicle/{uid} | delete the Vehicle with the given vehicleUid
[**getVehicle()**](VehicleManagerApi.md#getVehicle) | **GET** /manage/vehicle/{uid} | get the Vehicle with the given vehicleUid
[**getVehicleUids()**](VehicleManagerApi.md#getVehicleUids) | **GET** /manage/vehicle | get a list of all vehicleUids that belong to the current Dealer
[**getVehicleUidsWithFlatParameters()**](VehicleManagerApi.md#getVehicleUidsWithFlatParameters) | **GET** /manage/vehicle/flat_parameters | get a list of all vehicleUids that belong to the current Dealer
[**saveVehicle()**](VehicleManagerApi.md#saveVehicle) | **POST** /manage/vehicle | save a Vehicle


## `deleteAllVehicles()`

```php
deleteAllVehicles(): \SSIS\Vapi\Client\Model\ClientResponseCommonResponse
```

delete all Vehicles

delete all Vehicles

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->deleteAllVehicles();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->deleteAllVehicles: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseCommonResponse**](../Model/ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteAllVehiclesExcept()`

```php
deleteAllVehiclesExcept($delete_vehicles_by_uid_offernumber_except_request): \SSIS\Vapi\Client\Model\ClientResponseObject
```

delete all Vehicles except the ones with the given uids or offernumbers

delete all Vehicles except

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delete_vehicles_by_uid_offernumber_except_request = new \SSIS\Vapi\Client\Model\DeleteVehiclesByUidOffernumberExceptRequest(); // \SSIS\Vapi\Client\Model\DeleteVehiclesByUidOffernumberExceptRequest

try {
    $result = $apiInstance->deleteAllVehiclesExcept($delete_vehicles_by_uid_offernumber_except_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->deleteAllVehiclesExcept: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delete_vehicles_by_uid_offernumber_except_request** | [**\SSIS\Vapi\Client\Model\DeleteVehiclesByUidOffernumberExceptRequest**](../Model/DeleteVehiclesByUidOffernumberExceptRequest.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseObject**](../Model/ClientResponseObject.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteVehicleByOfferNumber()`

```php
deleteVehicleByOfferNumber($offer_number): \SSIS\Vapi\Client\Model\ClientResponseCommonResponse
```

delete the Vehicle with the given offerNumber

delete the Vehicle with the given offerNumber

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offer_number = 'offer_number_example'; // string

try {
    $result = $apiInstance->deleteVehicleByOfferNumber($offer_number);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->deleteVehicleByOfferNumber: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offer_number** | **string**|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseCommonResponse**](../Model/ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteVehicleByUid()`

```php
deleteVehicleByUid($uid): \SSIS\Vapi\Client\Model\ClientResponseCommonResponse
```

delete the Vehicle with the given vehicleUid

delete the Vehicle with the given vehicleUid

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uid = 'uid_example'; // string

try {
    $result = $apiInstance->deleteVehicleByUid($uid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->deleteVehicleByUid: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **string**|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseCommonResponse**](../Model/ClientResponseCommonResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVehicle()`

```php
getVehicle($uid): \SSIS\Vapi\Client\Model\ClientResponseManagerVehicle
```

get the Vehicle with the given vehicleUid

get the Vehicle with the given vehicle

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$uid = 'uid_example'; // string

try {
    $result = $apiInstance->getVehicle($uid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->getVehicle: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **string**|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseManagerVehicle**](../Model/ClientResponseManagerVehicle.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVehicleUids()`

```php
getVehicleUids($filter, $pagination): \SSIS\Vapi\Client\Model\ClientResponseVehicleUidList
```

get a list of all vehicleUids that belong to the current Dealer

get a list of all vehicleUids that belong to the current Dealer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter = new \SSIS\Vapi\Client\Model\\SSIS\Vapi\Client\Model\ManagerVehicleFilterParameters(); // \SSIS\Vapi\Client\Model\ManagerVehicleFilterParameters
$pagination = new \SSIS\Vapi\Client\Model\\SSIS\Vapi\Client\Model\PaginationParameters(); // \SSIS\Vapi\Client\Model\PaginationParameters

try {
    $result = $apiInstance->getVehicleUids($filter, $pagination);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->getVehicleUids: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter** | [**\SSIS\Vapi\Client\Model\ManagerVehicleFilterParameters**](../Model/.md)|  |
 **pagination** | [**\SSIS\Vapi\Client\Model\PaginationParameters**](../Model/.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseVehicleUidList**](../Model/ClientResponseVehicleUidList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVehicleUidsWithFlatParameters()`

```php
getVehicleUidsWithFlatParameters($offer_number, $changed_before, $changed_after, $send_before, $send_after, $has_errors, $make_key, $model_key, $min_dealer_price, $max_dealer_price, $min_consumer_price, $max_consumer_price, $result_count, $result_offset, $sort_type, $sort_order_asc): \SSIS\Vapi\Client\Model\ClientResponseVehicleUidList
```

get a list of all vehicleUids that belong to the current Dealer

get a list of all vehicleUids that belong to the current Dealer

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offer_number = 'offer_number_example'; // string
$changed_before = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime
$changed_after = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime
$send_before = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime
$send_after = new \DateTime("2013-10-20T19:20:30+01:00"); // \DateTime
$has_errors = True; // bool
$make_key = 'make_key_example'; // string
$model_key = 'model_key_example'; // string
$min_dealer_price = 3.4; // float
$max_dealer_price = 3.4; // float
$min_consumer_price = 3.4; // float
$max_consumer_price = 3.4; // float
$result_count = 56; // int
$result_offset = 56; // int
$sort_type = 'sort_type_example'; // string
$sort_order_asc = True; // bool

try {
    $result = $apiInstance->getVehicleUidsWithFlatParameters($offer_number, $changed_before, $changed_after, $send_before, $send_after, $has_errors, $make_key, $model_key, $min_dealer_price, $max_dealer_price, $min_consumer_price, $max_consumer_price, $result_count, $result_offset, $sort_type, $sort_order_asc);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->getVehicleUidsWithFlatParameters: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offer_number** | **string**|  | [optional]
 **changed_before** | **\DateTime**|  | [optional]
 **changed_after** | **\DateTime**|  | [optional]
 **send_before** | **\DateTime**|  | [optional]
 **send_after** | **\DateTime**|  | [optional]
 **has_errors** | **bool**|  | [optional]
 **make_key** | **string**|  | [optional]
 **model_key** | **string**|  | [optional]
 **min_dealer_price** | **float**|  | [optional]
 **max_dealer_price** | **float**|  | [optional]
 **min_consumer_price** | **float**|  | [optional]
 **max_consumer_price** | **float**|  | [optional]
 **result_count** | **int**|  | [optional]
 **result_offset** | **int**|  | [optional]
 **sort_type** | **string**|  | [optional]
 **sort_order_asc** | **bool**|  | [optional]

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseVehicleUidList**](../Model/ClientResponseVehicleUidList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `saveVehicle()`

```php
saveVehicle($manager_vehicle): \SSIS\Vapi\Client\Model\ClientResponseVehicleSaveResponse
```

save a Vehicle

save a Vehicle

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\VehicleManagerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$manager_vehicle = new \SSIS\Vapi\Client\Model\ManagerVehicle(); // \SSIS\Vapi\Client\Model\ManagerVehicle

try {
    $result = $apiInstance->saveVehicle($manager_vehicle);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling VehicleManagerApi->saveVehicle: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **manager_vehicle** | [**\SSIS\Vapi\Client\Model\ManagerVehicle**](../Model/ManagerVehicle.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseVehicleSaveResponse**](../Model/ClientResponseVehicleSaveResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

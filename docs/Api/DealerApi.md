# SSIS\Vapi\Client\DealerApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLoggedInDealerDetails()**](DealerApi.md#getLoggedInDealerDetails) | **GET** /dealer/info | DealerApi.info()


## `getLoggedInDealerDetails()`

```php
getLoggedInDealerDetails(): \SSIS\Vapi\Client\Model\ClientResponseIndexDealerDetailResponseList
```

DealerApi.info()

Get details for logged in dealer.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\DealerApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getLoggedInDealerDetails();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DealerApi->getLoggedInDealerDetails: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SSIS\Vapi\Client\Model\ClientResponseIndexDealerDetailResponseList**](../Model/ClientResponseIndexDealerDetailResponseList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json;charset=utf-8`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

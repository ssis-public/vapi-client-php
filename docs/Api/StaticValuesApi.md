# SSIS\Vapi\Client\StaticValuesApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllStaticValueClasses()**](StaticValuesApi.md#getAllStaticValueClasses) | **GET** /value | Get all class names for static values.
[**getModelsByMake()**](StaticValuesApi.md#getModelsByMake) | **GET** /value/models/{make} | Get all models of given make.
[**getStaticValueContentByKey()**](StaticValuesApi.md#getStaticValueContentByKey) | **GET** /value/{staticValueClassName}/{staticValueKey} | Get values of given static value class and key.
[**getStaticValueContents()**](StaticValuesApi.md#getStaticValueContents) | **GET** /value/{staticValueClassName} | Get all values of given static value class.


## `getAllStaticValueClasses()`

```php
getAllStaticValueClasses(): \SSIS\Vapi\Client\Model\ClassListResponse
```

Get all class names for static values.

Get all class names for static values.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\StaticValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getAllStaticValueClasses();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StaticValuesApi->getAllStaticValueClasses: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SSIS\Vapi\Client\Model\ClassListResponse**](../Model/ClassListResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getModelsByMake()`

```php
getModelsByMake($make): \SSIS\Vapi\Client\Model\ListResponseClientResponseIntf
```

Get all models of given make.

Get all models of given make.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\StaticValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$make = 'make_example'; // string

try {
    $result = $apiInstance->getModelsByMake($make);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StaticValuesApi->getModelsByMake: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **make** | **string**|  |

### Return type

[**\SSIS\Vapi\Client\Model\ListResponseClientResponseIntf**](../Model/ListResponseClientResponseIntf.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getStaticValueContentByKey()`

```php
getStaticValueContentByKey($static_value_class_name, $static_value_key): \SSIS\Vapi\Client\Model\GeneralResponse
```

Get values of given static value class and key.

Get values of given static value class and key.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\StaticValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$static_value_class_name = 'static_value_class_name_example'; // string
$static_value_key = 'static_value_key_example'; // string

try {
    $result = $apiInstance->getStaticValueContentByKey($static_value_class_name, $static_value_key);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StaticValuesApi->getStaticValueContentByKey: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **static_value_class_name** | **string**|  |
 **static_value_key** | **string**|  |

### Return type

[**\SSIS\Vapi\Client\Model\GeneralResponse**](../Model/GeneralResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getStaticValueContents()`

```php
getStaticValueContents($static_value_class_name): \SSIS\Vapi\Client\Model\ListResponseClientResponseIntf
```

Get all values of given static value class.

Get all values of given static value class.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\StaticValuesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$static_value_class_name = 'static_value_class_name_example'; // string

try {
    $result = $apiInstance->getStaticValueContents($static_value_class_name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StaticValuesApi->getStaticValueContents: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **static_value_class_name** | **string**|  |

### Return type

[**\SSIS\Vapi\Client\Model\ListResponseClientResponseIntf**](../Model/ListResponseClientResponseIntf.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

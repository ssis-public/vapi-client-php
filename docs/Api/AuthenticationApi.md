# SSIS\Vapi\Client\AuthenticationApi

All URIs are relative to https://vapi.ssis.de.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getNewToken()**](AuthenticationApi.md#getNewToken) | **GET** /public/getNewToken | Get a new Token only if the given Token is valid. Otherwise a new login is required.
[**login()**](AuthenticationApi.md#login) | **POST** /public/login | Login the given API-User and return a Token.


## `getNewToken()`

```php
getNewToken(): \SSIS\Vapi\Client\Model\JWTTokenResponse
```

Get a new Token only if the given Token is valid. Otherwise a new login is required.

Get a new Token only if the given Token is valid. Otherwise a new login is required.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getNewToken();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->getNewToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\SSIS\Vapi\Client\Model\JWTTokenResponse**](../Model/JWTTokenResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `login()`

```php
login($authentication_request): \SSIS\Vapi\Client\Model\JWTTokenResponse
```

Login the given API-User and return a Token.

Login the given API-User and return a Token.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = SSIS\Vapi\Client\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new SSIS\Vapi\Client\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$authentication_request = new \SSIS\Vapi\Client\Model\AuthenticationRequest(); // \SSIS\Vapi\Client\Model\AuthenticationRequest

try {
    $result = $apiInstance->login($authentication_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->login: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authentication_request** | [**\SSIS\Vapi\Client\Model\AuthenticationRequest**](../Model/AuthenticationRequest.md)|  |

### Return type

[**\SSIS\Vapi\Client\Model\JWTTokenResponse**](../Model/JWTTokenResponse.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

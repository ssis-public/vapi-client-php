# # IndexVehicleSearchResponseList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hits_found** | **int** | Total hits found. |
**hits_responded** | **int** | Hits responded, depends on total hits and your pagination parameters. |
**aggregations** | [**array<string,array<string,object>>**](array.md) |  | [optional]
**search_errors** | **string[]** | A list of search errors. | [optional]
**search_hits** | [**\SSIS\Vapi\Client\Model\IndexVehicleSearchResponseItem[]**](IndexVehicleSearchResponseItem.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # IndexVehicleSearchRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter** | [**\SSIS\Vapi\Client\Model\VehicleFilter**](VehicleFilter.md) |  | [optional]
**aggregation_types** | **string[]** | Define your aggregation. | [optional]
**pagination** | [**\SSIS\Vapi\Client\Model\PaginationParameters**](PaginationParameters.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

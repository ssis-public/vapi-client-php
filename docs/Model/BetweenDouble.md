# # BetweenDouble

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **double** | The begin value. | [optional]
**until** | **double** | The end value. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

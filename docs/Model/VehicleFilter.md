# # VehicleFilter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **string[]** | A list of uids, you want to search for. You can also use substrings of the uids. | [optional]
**last_change** | [**\SSIS\Vapi\Client\Model\BetweenOffsetDateTime**](BetweenOffsetDateTime.md) |  | [optional]
**last_update** | [**\SSIS\Vapi\Client\Model\BetweenOffsetDateTime**](BetweenOffsetDateTime.md) |  | [optional]
**valid_after** | [**\SSIS\Vapi\Client\Model\BetweenOffsetDateTime**](BetweenOffsetDateTime.md) |  | [optional]
**distributor_id** | **int[]** | The internal dealer ID of the distributor. | [optional]
**is_vehicle_owner** | **bool** | Dealer is the vehicle owner. | [optional]
**is_selected_by_reseller** | **bool** | This flag tells, if this vehicle is selected to show by the reseller. | [optional]
**selectable** | **bool** | This flag tells, if this vehicle is selectable. It is selectable when no errors from import exists. | [optional]
**offer_number** | **string[]** | The offer numbers of the vehicles. | [optional]
**model_search** | [**\SSIS\Vapi\Client\Model\VehicleModelSearch[]**](VehicleModelSearch.md) | Search for vehicle make, model and version. | [optional]
**model_search_exclude** | [**\SSIS\Vapi\Client\Model\VehicleModelSearch[]**](VehicleModelSearch.md) | Search for vehicle excluding make, model and version. The excludes are prioritized. | [optional]
**hsn** | **int[]** | Make key number. | [optional]
**tsn** | **string[]** | The model code number of the vehicle. | [optional]
**model_year** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**engine_power_kw** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**engine_cylinder_count** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**cubic_capacity** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**gear_count** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**transmission_type_key** | **string[]** | The transmission type key of the vehicle. Valid values are in static value class Transmission. | [optional]
**axe_count** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**wheel_base** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**tire_size** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**drive_type_key** | **string[]** | The drive type key. Valid values are in static value class DriveType. | [optional]
**manufacturing_year** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**body_style_key** | **string[]** | Style key of the vehicle body. Valid values are in static value class BodyStyle. | [optional]
**door_count** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**seat_count** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**vin** | **string[]** | Chassis number. | [optional]
**purchase_price** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**sales_price** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**gross_sales_price** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**tax_reportable** | **bool** | This flag tells if the prices are net or gross. If true then the prices are net. | [optional]
**broker_offer** | **bool** | The flag tells if the offer was done by a broker. | [optional]
**agecategory_key** | **string[]** | The age category key of the vehicle. Valid values are in static value class Agecategory. | [optional]
**deliverycategory_key** | **string[]** | The delivery category key. Valid values are in static value class Deliverycategory. | [optional]
**delivery_date** | [**\SSIS\Vapi\Client\Model\BetweenOffsetDateTime**](BetweenOffsetDateTime.md) |  | [optional]
**delivery_time** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**retrieval_country** | **string[]** | The retrieval country of the vehicle. (ISO2).  Valid values are in static value class Country. | [optional]
**origin_country_key** | **string[]** | The origin country of the vehicle. (ISO2).  Valid values are in static value class Country. | [optional]
**reimport** | **bool** | Reimport vehicle. | [optional]
**mileage** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**previous_owner_count** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**one_day_registration** | **bool** | Has a one day registration. | [optional]
**first_registration_date** | [**\SSIS\Vapi\Client\Model\BetweenLocalDate**](BetweenLocalDate.md) |  | [optional]
**warranty_start_date** | [**\SSIS\Vapi\Client\Model\BetweenLocalDate**](BetweenLocalDate.md) |  | [optional]
**accidented** | **bool** | This flag tells if the vehicle had an accident. | [optional]
**functional** | **bool** | Ready for driving. | [optional]
**returned_rental** | **bool** | Rental returned. | [optional]
**returned_leasing** | **bool** | Leasing returned. | [optional]
**service_record** | **bool** | Has a service record. | [optional]
**general_inspection** | [**\SSIS\Vapi\Client\Model\BetweenLocalDate**](BetweenLocalDate.md) |  | [optional]
**nonsmoker** | **bool** | Auction is hidden. | [optional]
**weight** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**coc_mass** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**trailer_load** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**yearly_tax** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**energy_efficiency_class_key** | **string[]** | The label key for the applicable class of energy consumption, ranges between A+, A, B, C, D, E, F or G. Valid values are in static value class EnergyEfficiencyClass. | [optional]
**energy_efficiency_class** | **string[]** | The label value for the applicable class of energy consumption, ranges between A+, A, B, C, D, E, F or G. Valid values are in static value class EnergyEfficiencyClass. | [optional]
**nedc_emission** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**nedc_consumption_inner** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**nedc_consumption_outer** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**nedc_consumption_combined** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**nedc_consumption_power_combined** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**wltp_emission** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**wltp_consumption_combined** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**wltp_consumption_low** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**wltp_consumption_medium** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**wltp_consumption_high** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**wltp_consumption_extra_high** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**fuel_type_key** | **string[]** | The fuel type key. See https://vapi.ssis.de/value/FuelType for possible values. | [optional]
**primary_fuel_key** | **string[]** | The primary fuel key. See https://vapi.ssis.de/value/Fuel for possible values. | [optional]
**secondary_fuel_key** | **string[]** | The fuel type detail. See https://vapi.ssis.de/value/FuelTypeDetail for possible values. | [optional]
**emission_sticker_key** | **string[]** | Emission sticker for German low emission zones (Feinstaubplakette f�r Umweltzone). Valid keys are in static value class EmissionSticker. | [optional]
**emission_standard_key** | **string[]** | EURO 1, 2, 3, 4, ... . Valid keys are in static value class EmissionStandard. | [optional]
**interior_color_key** | **string[]** | The interior color key. Valid values are in static value class Color. | [optional]
**upholstery_key** | **string[]** | Upholstery type. Valid values are in static value class Upholstery. | [optional]
**cargo_space_length** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**cargo_space_width** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**cargo_space_height** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**exterior** | [**\SSIS\Vapi\Client\Model\VehicleExteriorSearch[]**](VehicleExteriorSearch.md) | The exterior color and paint combination. | [optional]
**min_internal_image_count** | **int** | Minimum count of uncensored images attached to this vehicle. | [optional]
**min_external_image_count** | **int** | Minimum count of censored images attached to this vehicle. | [optional]
**searchflags** | **string[]** | Searchflags for additional vehicle features. | [optional]
**no_previous_registration** | **bool** | Vehicle has no previous registration. | [optional]
**deliverable_immediately** | **bool** | Deliverable immediately. | [optional]
**deliverable_on_short_notice** | **bool** | Deliverable on short notice. | [optional]
**vehicle_type** | **string[]** | Type of indexed vehicle, e.g. vehicle, outlet | [optional]
**warrantyable** | **bool** | Warrantyable. | [optional]
**date_indexed** | [**\SSIS\Vapi\Client\Model\BetweenOffsetDateTime**](BetweenOffsetDateTime.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # PaginationParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result_count** | **int** | Number of datasets returns. | [optional] [default to 10]
**result_offset** | **int** | Dataset to begin. | [optional] [default to 0]
**sort_type** | **string** | The column name to sort by. Most fields are supported for sorting | [optional] [default to 'purchasePrice']
**sort_order_asc** | **bool** | The sort direction. If true then ascending. | [optional] [default to true]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

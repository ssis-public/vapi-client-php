# # IndexDealerProduct

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | The unique key of the product. | [optional]
**name** | **string** | The name of the product. | [optional]
**description** | **string** | The description of the product. | [optional]
**expire** | [**\DateTime**](\DateTime.md) | The expiring date of the product. | [optional]
**status** | [**\SSIS\Vapi\Client\Model\IndexDealerProductStatus**](IndexDealerProductStatus.md) |  | [optional]
**options** | [**\SSIS\Vapi\Client\Model\IndexDealerProductOption[]**](IndexDealerProductOption.md) | Further options related to the product. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

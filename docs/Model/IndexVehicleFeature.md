# # IndexVehicleFeature

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **string** |  | [optional]
**text** | **string** |  | [optional]
**purchase_price** | **double** |  | [optional]
**net_sales_price** | **double** |  | [optional]
**gross_sales_price** | **double** |  | [optional]
**blacklisted** | **bool** |  | [optional]
**searchflags** | **string[]** |  | [optional]
**sales_price** | **double** |  | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

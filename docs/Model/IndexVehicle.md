# # IndexVehicle

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**control_field** | **string** | This field is used for internal debugging. | [optional]
**uid** | **string** | The internal uid. Was generated by the VAPI powercalc. | [optional]
**last_change** | [**\DateTime**](\DateTime.md) | The timestamp of the last modifications for this vehicle. | [optional]
**valid_after** | [**\DateTime**](\DateTime.md) | The vehicle offer is valid after this timestamp. | [optional]
**distributor_id** | **int** | The internal dealer ID of the distributor. | [optional]
**reseller_id** | **int** | The internal dealer ID of the reseller. | [optional]
**is_vehicle_owner** | **bool** | Dealer is the vehicle owner. | [optional]
**is_selected_by_reseller** | **bool** | This flag tells, if this vehicle is selected to show by the reseller. | [optional]
**selectable** | **bool** | This flag tells, if this vehicle is selectable. It is selectable when no errors from import exists. | [optional]
**native_offer_number** | **string** | The offer number of the vehicle. | [optional]
**offer_number** | **string** | This is an unique offer number of the vehicle, set by VAPI. | [optional]
**make_text** | **string** | Make text. | [optional]
**make_key** | **string** | The makeKey of this vehicle. | [optional]
**model_text** | **string** | The model of this vehicle. (Free text from distributor) | [optional]
**model_key** | **string** | The modelKey of this vehicle. | [optional]
**version_text** | **string** | The version of this vehicle model. | [optional]
**hsn** | **int** | Make key number. | [optional]
**tsn** | **string** | The model code number of the vehicle. | [optional]
**model_year** | **int** | The model year. | [optional]
**engine_power_kw** | **int** | Engine power in kW. | [optional]
**engine_cylinder_count** | **int** | Number of engine cylinders. | [optional]
**cubic_capacity** | **int** | The cubic capacity. | [optional]
**gear_count** | **int** | Number of gears. | [optional]
**transmission_type_key** | **string** | The transmission type key of the vehicle. Valid values are in static value class Transmission. | [optional]
**transmission_type** | **string** | The transmission type value of the vehicle. Valid values are in static value class Transmission. | [optional]
**axe_count** | **int** | Number of axes. | [optional]
**wheel_base** | **int** | Wheelbase. | [optional]
**tire_size** | **int** | Tire Size. | [optional]
**drive_type_key** | **string** | The drive type key. Valid values are in static value class Drivetype. | [optional]
**drive_type_text** | **string** | The drive type value. Valid values are in static value class Drivetype. | [optional]
**manufacturing_year** | **int** | Year of Manufacture. | [optional]
**body_style_key** | **string** | Style key of the vehicle body. Valid values are in static value class BodyStyle. | [optional]
**body_style_text** | **string** | Style of the vehicle body. Valid values are in static value class BodyStyle. | [optional]
**door_count** | **int** | Number of doors. | [optional]
**seat_count** | **int** | The number of seats. | [optional]
**vin** | **string** | Chassis number. | [optional]
**purchase_price** | **double** | The net purchase price of this vehicle. | [optional]
**sales_price** | **double** | The net sales price of this vehicle. | [optional]
**gross_sales_price** | **double** | The gross sales price of this vehicle. | [optional]
**calculation_info** | [**\SSIS\Vapi\Client\Model\CalculationInfo**](CalculationInfo.md) |  | [optional]
**tax_reportable** | **bool** | This flag tells if the prices are net or gross. If true then the prices are net. | [optional]
**broker_offer** | **bool** | The flag tells if the offer was done by a broker. | [optional]
**agecategory_key** | **string** | The age category key of the vehicle. Valid values are in static value class Agecategory. | [optional]
**agecategory_text** | **string** | The age category value of the vehicle. Valid values are in static value class Agecategory. | [optional]
**deliverycategory_key** | **string** | The delivery category key. Valid values are in static value class Deliverycategory. | [optional]
**deliverycategory_text** | **string** | The delivery category value. Valid values are in static value class Deliverycategory. | [optional]
**delivery_date** | [**\DateTime**](\DateTime.md) | The delivery date of the vehicle. | [optional]
**delivery_time** | **int** | The delivery time of the vehicle. | [optional]
**origin_country_key** | **string** | The origin country of the vehicle. (ISO2).  Valid values are in static value class Country. | [optional]
**origin_country_text** | **string** | The origin country of the vehicle. (ISO2).  Valid values are in static value class Country. | [optional]
**reimport** | **bool** | Reimport vehicle. | [optional]
**mileage** | **int** | The mileage. | [optional]
**previous_owner_count** | **int** | Number of previous owners. | [optional]
**one_day_registration** | **bool** | Has a one day registration. | [optional]
**first_registration_date** | [**\DateTime**](\DateTime.md) | The date of the vehicle first registration. | [optional]
**warranty_start_date** | [**\DateTime**](\DateTime.md) | The start date of the warranty | [optional]
**accidented** | **bool** | This flag tells if the vehicle had an accident. | [optional]
**functional** | **bool** | Ready for driving. | [optional]
**returned_rental** | **bool** | Rental returned. | [optional]
**returned_leasing** | **bool** | Leasing returned. | [optional]
**service_record** | **bool** | Has a service record. | [optional]
**general_inspection** | [**\DateTime**](\DateTime.md) | The Date for the next general inspection. | [optional]
**non_smoker** | **bool** | True, if the vehicle has not been smoked in. | [optional]
**weight** | **int** | The empty weight of the vehicle in kg. | [optional]
**coc_mass** | **int** | Certificate of Conformity mass. | [optional]
**trailer_load** | **int** | The maximum trailer load. | [optional]
**energy_efficiency_class_key** | **string** | The label for the applicable class of energy consumption, ranges between A+, A, B, C, D, E, F or G. Valid values are in static value class EnergyEfficiencyClass. | [optional]
**energy_efficiency_class** | **string** | The label for the applicable class of energy consumption, ranges between A+, A, B, C, D, E, F or G. Valid values are in static value class EnergyEfficiencyClass. | [optional]
**nedc_emission** | **double** | Indicates the amount of carbon dioxide emissions (Co2) in grams per kilometer traveled. | [optional]
**nedc_consumption_inner** | **double** | Fuel consumption as measured in specific tests executed in city traffic situations. Number in l/100km (natural gas (CNG) in kg/100km, is ignored for electric vehicles) | [optional]
**nedc_consumption_outer** | **double** | Fuel consumption as measured in specific tests executed in highway traffic situations. Number in l/100km (natural gas (CNG) in kg/100km, is ignored for electric vehicles) | [optional]
**nedc_consumption_combined** | **double** | Fuel consumption as measured in specific tests executed in city and highway traffic situations. Number in l/100km (natural gas (CNG) in kg/100km, electric vehicles in kWh/100km) | [optional]
**nedc_consumption_power_combined** | **double** | Power consumption of hybrid vehicles in kWh/100km | [optional]
**wltp_emission** | **int** | WLTP Emission in g/km. | [optional]
**wltp_consumption_combined** | **double** | WLTP Consumption Combined in l/100km (gas in kg/100km - electric in kWh/100km). | [optional]
**wltp_consumption_low** | **double** | WLTP Consumption Low in l/100km (gas in kg/100km - electric in kWh/100km). | [optional]
**wltp_consumption_medium** | **double** | WLTP Consumption Medium in l/100km (gas in kg/100km - electric in kWh/100km). | [optional]
**wltp_consumption_high** | **double** | WLTP Consumption High in l/100km (gas in kg/100km - electric in kWh/100km). | [optional]
**wltp_consumption_extra_high** | **double** | WLTP Consumption Extra High in l/100km (gas in kg/100km - electric in kWh/100km). | [optional]
**fuel_type_key** | **string** | The fuelType category key. Valid values are in static value class Fueltype. | [optional]
**fuel_type_text** | **string** | The fuelType category value. Valid values are in static value class Fueltype. | [optional]
**primary_fuel_key** | **string** | When you want to provide EnVKV compliant values, for fuel type petrol, you have to specify the exact petrol type, that the consumption values are based on petrol types. Valid values are in static value class FueltypeDetail. | [optional]
**primary_fuel_text** | **string** | When you want to provide EnVKV compliant values, for fuel type petrol, you have to specify the exact petrol type, that the consumption values are based on petrol types. Valid values are in static value class FueltypeDetail. | [optional]
**primary_fuel_unit** | **string** | The unit of the petrol type. Valid values are in static value class FueltypeDetail. | [optional]
**secondary_fuel_key** | **string** | When you want to provide EnVKV compliant values, for fuel type petrol, you have to specify the exact petrol type, that the consumption values are based on petrol types. Valid values are in static value class FueltypeDetail. | [optional]
**secondary_fuel_text** | **string** | When you want to provide EnVKV compliant values, for fuel type petrol, you have to specify the exact petrol type, that the consumption values are based on petrol types. Valid values are in static value class FueltypeDetail. | [optional]
**secondary_fuel_unit** | **string** | The unit of the petrol type. Valid values are in static value class FueltypeDetail. | [optional]
**emission_sticker_key** | **string** | Emission sticker for German low emission zones (Feinstaubplakette fuer Umweltzone). Valid keys are in static value class EmissionSticker. | [optional]
**emission_sticker_text** | **string** | Emission sticker for German low emission zones (Feinstaubplakette fuer Umweltzone). Valid values are in static value class EmissionSticker. | [optional]
**emission_standard_key** | **string** | EURO 1, 2, 3, 4, ... . Valid keys are in static value class EmissionSticker. | [optional]
**emission_standard_text** | **string** | EURO 1, 2, 3, 4, ... . Valid values are in static value class EmissionSticker. | [optional]
**interior_color_key** | **string** | The interior color key. Valid values are in static value class Color. | [optional]
**interior_color_text** | **string** | The interior color text (Free text from distributor). | [optional]
**upholstery_key** | **string** | Upholstery key. Valid values are in static value class Upholstery. | [optional]
**upholstery_text** | **string** | Upholstery text. | [optional]
**cargo_space_length** | **int** | The cargo space length in mm. | [optional]
**cargo_space_width** | **int** | The cargo space width in mm. | [optional]
**cargo_space_height** | **int** | The cargo space height in mm. | [optional]
**additional_notes** | **string** | Additional notes. | [optional]
**data_provider_vehicle_id** | **string** | If the vehicle comes from a provider, his vehicle ID. | [optional]
**data_provider_vehicle_url** | **string** | If the vehicle comes from a provider, his vehicle URL. | [optional]
**exteriors** | [**\SSIS\Vapi\Client\Model\IndexVehicleExterior[]**](IndexVehicleExterior.md) | A vehicle can have multiple exteriors. One exterior has fields fields for: colorKey, paintKey, text, hexcode, price. | [optional]
**exterior_keys** | **string[]** | A list of exterior keys. This value is used to search for vehicles by their exterior. | [optional]
**internal_images** | [**\SSIS\Vapi\Client\Model\IndexVehicleImage[]**](IndexVehicleImage.md) | The uncensored vehicle images. | [optional]
**external_images** | [**\SSIS\Vapi\Client\Model\IndexVehicleImage[]**](IndexVehicleImage.md) | The censored vehicle images. | [optional]
**internal_image_count** | **int** | The number of uncensored images attached to this vehicle. | [optional]
**external_image_count** | **int** | The number of censored images attached to this vehicle. | [optional]
**features** | [**\SSIS\Vapi\Client\Model\IndexVehicleFeature[]**](IndexVehicleFeature.md) | Additional vehicle features. | [optional]
**changes** | [**\SSIS\Vapi\Client\Model\Change[]**](Change.md) | All changes to the vehicle data. | [optional]
**errors** | [**\SSIS\Vapi\Client\Model\Error[]**](Error.md) | All errors in the vehicle data. | [optional]
**no_previous_registration** | **bool** | Vehicle has no previous registration. | [optional]
**deliverable_immediately** | **bool** | Deliverable immediately. | [optional]
**deliverable_on_short_notice** | **bool** | Deliverable on short notice. | [optional]
**vehicle_type** | **string** | Type of indexed vehicle, e.g. vehicle, outlet | [optional] [default to 'vehicle']
**data_export_forbidden** | **bool** | The Export of this vehicles data to other markets is forbidden. | [optional]
**warranties** | [**\SSIS\Vapi\Client\Model\IndexVehicleWarranty[]**](IndexVehicleWarranty.md) | The warranty durations and prices. | [optional]
**date_indexed** | [**\DateTime**](\DateTime.md) | Vehicle indexed timestamp. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # CalculationInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distributor_selected** | **bool** |  | [optional]
**price_gap_selected** | **bool** |  | [optional]
**model_selected** | **bool** |  | [optional]
**vat_status_selected** | **bool** |  | [optional]
**picture_status_selected** | **bool** |  | [optional]
**gross_purchase_price** | **double** |  | [optional]
**distributor_surcharge** | **double** |  | [optional]
**price_group_surcharge** | **double** |  | [optional]
**warranty_surcharge** | **double** |  | [optional]
**rounding** | **int** |  | [optional]
**reduction** | **int** |  | [optional]
**feature_surcharge** | **double** |  | [optional]
**feature_rounding** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

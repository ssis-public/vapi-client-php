# # Exterior

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color_key** | **string** |  | [optional]
**paint_key** | **string** |  | [optional]
**text** | **string** |  | [optional]
**hexcode** | **string** |  | [optional]
**price** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # IndexVehicleDetailResponseList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**\SSIS\Vapi\Client\Model\IndexVehicle[]**](IndexVehicle.md) | List of Vehicles Details. | [optional]
**duplicates_removed** | **int** | Number of duplicate uids which was sent, has been removed. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

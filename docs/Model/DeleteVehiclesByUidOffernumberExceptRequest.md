# # DeleteVehiclesByUidOffernumberExceptRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uids** | **string[]** | The vehicles with these Uids should not be deleted. | [optional]
**offer_numbers** | **string[]** | The vehicles with these offer number should not be deleted. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InquiryRequestor

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealer_id** | **int** |  | [optional]
**requestor_type** | **string** | Contains the request values. | [optional]
**companyname** | **string** | If you are a company. | [optional]
**requestor_salutation** | **string** | Contains the request values. | [optional]
**firstname** | **string** |  | [optional]
**lastname** | **string** |  |
**street** | **string** |  | [optional]
**zipcode** | **string** |  | [optional]
**city** | **string** |  | [optional]
**country_iso2** | **string** |  | [optional]
**email** | **string** |  |
**phone** | **string** |  | [optional]
**fax** | **string** |  | [optional]
**mobile** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

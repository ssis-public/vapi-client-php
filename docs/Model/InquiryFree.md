# # InquiryFree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**make_key** | **string** | The key of a make. | [optional]
**model_key** | **string** | The key of a model. | [optional]
**version_text** | **string** | A further specification of the model. | [optional]
**deliverycategory_key** | **string[]** | The delivery category key. Valid values are in static value class Deliverycategory. | [optional]
**first_registration_date** | [**\SSIS\Vapi\Client\Model\BetweenLocalDate**](BetweenLocalDate.md) |  | [optional]
**sales_price** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**fuel_type_key** | **string[]** | The fuel type key. Valid values are in static value class FuelType. | [optional]
**consumption_combined** | [**\SSIS\Vapi\Client\Model\BetweenDouble**](BetweenDouble.md) |  | [optional]
**transmission_type_key** | **string[]** | The transmission type key of the vehicle. Valid values are in static value class Transmission. | [optional]
**mileage** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**door_count** | [**\SSIS\Vapi\Client\Model\BetweenInteger**](BetweenInteger.md) |  | [optional]
**engine_power_kw** | [**\SSIS\Vapi\Client\Model\BetweenShort**](BetweenShort.md) |  | [optional]
**exterior_color** | [**\SSIS\Vapi\Client\Model\VehicleExteriorSearch[]**](VehicleExteriorSearch.md) | The exterior color and paint combination. | [optional]
**features_json** | **string** | Only selected. Eg. {\&quot;Einparkhilfe - Selbstlenkende Systeme\&quot;: true} | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

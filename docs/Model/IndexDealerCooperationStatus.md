# # IndexDealerCooperationStatus

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | The unique key of the cooperation status. | [optional]
**name** | **string** | The name of the cooperation status. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # InquiryRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requestor_dealer_id** | **int** | Leave empty, if the request comes from end customer. | [optional]
**requested_dealer_id** | **int** | Leave empty, if the request goes to all dealers. | [optional]
**inquiry_type** | **string** | The inquiry type. Possible values: INQUIRY, OFFER | [default to 'INQUIRY']
**inquiry_subtype** | **string** | The inquiry subtype. Possible values: INQUIRY_DIRECT_PUBLIC, INQUIRY_DIRECT_INTERN, INQUIRY_FREE_PUBLIC, INQUIRY_FREE_INTERN |
**inquiry_requestor** | [**\SSIS\Vapi\Client\Model\InquiryRequestor**](InquiryRequestor.md) |  |
**inquiry_vehicle** | [**\SSIS\Vapi\Client\Model\InquiryVehicle**](InquiryVehicle.md) |  | [optional]
**inquiry_free** | [**\SSIS\Vapi\Client\Model\InquiryFree**](InquiryFree.md) |  | [optional]
**time_of_purchase** | **string** | When would the interested person like to buy the vehicle. | [optional]
**customer_message** | **string** | An additional message from the customer. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

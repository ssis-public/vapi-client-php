# # IndexDealer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | The internal elastic id. |
**company** | **string** | The company name of the dealer. |
**company_additional** | **string** | Additional company information. | [optional]
**street** | **string** | The street of the company address. | [optional]
**zip** | **string** | The zipcode of the company address. | [optional]
**city** | **string** | The city of the company address. | [optional]
**country_key** | **string** | The country of the company address. ISO 3166-1 alpha-2 format | [optional]
**country_text** | **string** | The country of the company address. | [optional]
**currency_iso** | **string** | The currency of the dealer. | [optional]
**currency_symbol** | **string** | The currency symbol of the deaker. | [optional]
**geo_location** | [**\SSIS\Vapi\Client\Model\Geolocation**](Geolocation.md) |  | [optional]
**phone** | **string** | The phone number of the dealer. | [optional]
**fax** | **string** | The fax number of the dealer. | [optional]
**email** | **string** | The email address of the dealer. | [optional]
**web** | **string** | The web address of the dealer. | [optional]
**business_organisation** | **string** | The type of the business organisation. | [optional]
**ceo** | **string** | The name of the CEO. | [optional]
**vatin** | **string** | The VAT Id of the company. | [optional]
**commercial_register_number** | **string** | The commercial register number of the company. | [optional]
**commercial_register_court** | **string** | The commercial register court of the company. | [optional]
**arbitration_office** | **string** | The arbitration office of the company. | [optional]
**insurance_broker_register_number** | **string** | The commercial register court of the company. | [optional]
**bic** | **string** | The BIC of the companies bank account. | [optional]
**iban** | **string** | The IBAN of the companies bank account. | [optional]
**bank** | **string** | The name of the companies bank account. | [optional]
**vat** | **float** | The VAT used for dealer&#39;s vehicles. | [optional]
**additional_information** | **string** | Additional information. | [optional]
**products** | [**\SSIS\Vapi\Client\Model\IndexDealerProduct[]**](IndexDealerProduct.md) | Dealer products. | [optional]
**contacts** | [**\SSIS\Vapi\Client\Model\IndexDealerContact[]**](IndexDealerContact.md) | Dealer contacts. | [optional]
**cooperations** | [**\SSIS\Vapi\Client\Model\IndexDealerCooperation[]**](IndexDealerCooperation.md) | Dealer cooperations. | [optional]
**settings** | [**\SSIS\Vapi\Client\Model\IndexDealerSetting[]**](IndexDealerSetting.md) | Dealer settings. | [optional]
**date_indexed** | [**\DateTime**](\DateTime.md) | Dealer indexed timestamp. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

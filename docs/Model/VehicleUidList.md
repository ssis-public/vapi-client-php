# # VehicleUidList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**vehicles** | [**\SSIS\Vapi\Client\Model\VehicleUidListItem[]**](VehicleUidListItem.md) |  | [optional]
**count** | **int** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

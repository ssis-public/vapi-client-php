# # IndexVehicleExterior

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color_key** | **string** |  | [optional]
**color_text** | **string** |  | [optional]
**paint_key** | **string** |  | [optional]
**paint_text** | **string** |  | [optional]
**text** | **string** |  | [optional]
**hex_code** | **string** |  | [optional]
**purchase_price** | **double** |  | [optional]
**net_sales_price** | **double** |  | [optional]
**gross_sales_price** | **double** |  | [optional]
**sales_price** | **double** | The net sales price | [optional] [readonly]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

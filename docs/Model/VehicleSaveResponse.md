# # VehicleSaveResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **string** |  | [optional]
**changes** | [**\SSIS\Vapi\Client\Model\Change[]**](Change.md) |  | [optional]
**errors** | [**\SSIS\Vapi\Client\Model\Error[]**](Error.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

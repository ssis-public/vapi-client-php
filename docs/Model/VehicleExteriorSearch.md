# # VehicleExteriorSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**color_key** | **string** | The extrior color key. Valid values are in static value class Color. | [optional]
**paint_key** | **string** | The extrior paint key. Valid values are in static value class Paint. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

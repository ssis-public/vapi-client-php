# # ClientResponseObject

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta_data** | [**\SSIS\Vapi\Client\Model\MetaData**](MetaData.md) |  |
**response** | **object** | The object with the payload data. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # IndexDealerDetailResponseList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dealers** | [**\SSIS\Vapi\Client\Model\IndexDealer[]**](IndexDealer.md) |  | [optional]
**errors** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

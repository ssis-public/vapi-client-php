# # Geolocation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **double** | The latitude geo coordinate of the location. | [optional]
**lon** | **double** | The longitude geo coordinate of the location. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

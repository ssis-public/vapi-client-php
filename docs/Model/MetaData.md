# # MetaData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**duration** | **string** | The time the request took. |
**request_id** | **string** | The internal request ID. |
**host_address** | **string** | The address of the host. |
**request_timestamp** | [**\DateTime**](\DateTime.md) | The timestamp of the request. |
**version** | **string** | The version number of the API. |
**version_timestamp** | [**\DateTime**](\DateTime.md) | The API version timestamp. |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

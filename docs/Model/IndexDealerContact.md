# # IndexDealerContact

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**order** | **int** | Contact order of the person. | [optional]
**title** | **string** | Title of the contact. | [optional]
**name** | **string** | Name of the contact. | [optional]
**email** | **string** | The email. | [optional]
**phone** | **string** | The phone number. | [optional]
**fax** | **string** | The fax number. | [optional]
**mobile** | **string** | The mobile number. | [optional]
**image_url** | **string** | An image url of a photo of the contact. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

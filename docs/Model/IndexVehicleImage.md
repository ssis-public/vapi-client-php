# # IndexVehicleImage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**small** | **string** |  | [optional]
**medium** | **string** |  | [optional]
**large** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

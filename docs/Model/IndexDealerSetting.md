# # IndexDealerSetting

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | A unique key of the setting. | [optional]
**description** | **string** | The description of the setting. | [optional]
**dealer_value** | **string** | The specific dealer setting value. | [optional]
**default_value** | **string** | A default value for this setting. | [optional]
**last_update** | [**\DateTime**](\DateTime.md) | The last update date of this setting. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

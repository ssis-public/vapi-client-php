# # IndexDealerCooperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | The unique key of the cooperation. | [optional]
**name** | **string** | The name of the cooperation. | [optional]
**status** | [**\SSIS\Vapi\Client\Model\IndexDealerCooperationStatus**](IndexDealerCooperationStatus.md) |  | [optional]
**options** | [**\SSIS\Vapi\Client\Model\IndexDealerCooperationOption[]**](IndexDealerCooperationOption.md) | Further options related to the cooperation. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # VehicleModelSearch

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**make_key** | **string** | The key of a make. | [optional]
**model_key** | **string** | The key of a model. | [optional]
**version_text** | **string** | A list of versions. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

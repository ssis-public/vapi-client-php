# # Error

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rule** | **string** |  | [optional]
**key** | **string** |  | [optional]
**message** | **string** |  | [optional]
**rejected** | **bool** |  | [optional]
**fields** | **array<string,string>** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

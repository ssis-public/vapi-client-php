# # VehicleUidListItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **string** |  | [optional]
**date_modified** | [**\DateTime**](\DateTime.md) |  | [optional]
**last_update** | [**\DateTime**](\DateTime.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

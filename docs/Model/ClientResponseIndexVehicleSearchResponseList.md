# # ClientResponseIndexVehicleSearchResponseList

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**meta_data** | [**\SSIS\Vapi\Client\Model\MetaData**](MetaData.md) |  |
**response** | [**\SSIS\Vapi\Client\Model\IndexVehicleSearchResponseList**](IndexVehicleSearchResponseList.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # BetweenOffsetDateTime

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | [**\DateTime**](\DateTime.md) | The begin value. | [optional]
**until** | [**\DateTime**](\DateTime.md) | The end value. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

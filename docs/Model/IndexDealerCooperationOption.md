# # IndexDealerCooperationOption

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | The unique key of the cooperation option. | [optional]
**name** | **string** | The name of the cooperation option. | [optional]
**value** | **string** | The value of the cooperation option. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # ManagerVehicleFilterParameters

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offer_number** | **string** |  | [optional]
**changed_before** | [**\DateTime**](\DateTime.md) |  | [optional]
**changed_after** | [**\DateTime**](\DateTime.md) |  | [optional]
**send_before** | [**\DateTime**](\DateTime.md) |  | [optional]
**send_after** | [**\DateTime**](\DateTime.md) |  | [optional]
**has_errors** | **bool** |  | [optional]
**make_key** | **string** |  | [optional]
**model_key** | **string** |  | [optional]
**min_dealer_price** | **float** |  | [optional]
**max_dealer_price** | **float** |  | [optional]
**min_consumer_price** | **float** |  | [optional]
**max_consumer_price** | **float** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

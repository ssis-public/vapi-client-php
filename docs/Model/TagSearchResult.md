# # TagSearchResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aggregation** | [**array<string,array<string,\SSIS\Vapi\Client\Model\TagObject>>**](array.md) | The aggragations. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

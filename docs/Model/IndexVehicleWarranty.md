# # IndexVehicleWarranty

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warranty_duration** | **int** | The duration of the warranty. | [optional]
**price** | **double** | The price of the warranty. | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)

# # IndexVehicleSearchResponseItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**uid** | **string** | The internal UID of the vehicle. |
**date_indexed** | [**\DateTime**](\DateTime.md) | The timestamp the vehicle was indexed. |
**vehicle_type_key** | **string** | Type of indexed vehicle, e.g. vehicle, outlet | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
